package com.rzj.b2b.commonmodule.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Classname Cart
 * @Description TODO
 * @Date 2021-6-9
 * @Created by rzj
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Goods implements Serializable {

    public int gid, gprice;

    public String gname, gdetails;

    private  Integer types;

    private String gremain;

}
