package com.rzj.b2b.commonmodule.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Classname Cart
 * @Description TODO
 * @Date 2021-6-9
 * @Created by rzj
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Cart implements Serializable {

    private Integer id;

    private String goodsname;

    private Integer number;

    private Integer price;

    private Integer goodid;

    private Integer uid;

}
