package com.rzj.b2b.commonmodule.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @program: store
 * @description: 折扣信息
 * @author: 作者
 * @create: 2021-06-22 16:43
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Discount implements Serializable {

    private int did;

    private String gname;

    private BigDecimal discount;

    private Date starTime;

    private Date endTime;

}
