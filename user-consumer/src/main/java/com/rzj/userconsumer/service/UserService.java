package com.rzj.userconsumer.service;


import com.rzj.b2b.commonmodule.model.User;
import com.rzj.userconsumer.hystrix.UserServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Classname LoginService
 * @Description TODO
 * Created by rzj on 2021/6/9
 */
@Component
@FeignClient(value = "b2b-user-provider", fallback = UserServiceHystrix.class)
public interface UserService {

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    User login(@RequestParam(value = "uname") String uname);

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    int register(@RequestParam(value = "upassword") String upassword,
                 @RequestParam(value = "uname") String uname,
                 @RequestParam(value = "usex") String usex,
                 @RequestParam(value = "uphone") String uphone,
                 @RequestParam(value = "unational") String unational);

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    int addUser(@RequestParam(value = "upassword") String upassword,
                @RequestParam(value = "uname") String uname,
                @RequestParam(value = "usex") String usex,
                @RequestParam(value = "uphone") String uphone,
                @RequestParam(value = "unational") String unational);

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    int updateUser(@RequestParam(value = "uaccount") String uaccount,
                   @RequestParam(value = "upassword") String upassword,
                   @RequestParam(value = "uname") String uname,
                   @RequestParam(value = "usex") String usex,
                   @RequestParam(value = "uphone") String uphone,
                   @RequestParam(value = "unational") String unational);

    @RequestMapping(value = "/delUser", method = RequestMethod.POST)
    int delUser(@RequestParam(value = "uaccount") String uaccount);

    @RequestMapping(value = "/userList", method = RequestMethod.POST)
    List<User> findUserByType(@RequestParam(value = "utype") Integer utype);

    @RequestMapping(value = "/user/{uaccount}", method = RequestMethod.POST)
    User findUserByID(@PathVariable(value = "uaccount") String uaccount);

}
