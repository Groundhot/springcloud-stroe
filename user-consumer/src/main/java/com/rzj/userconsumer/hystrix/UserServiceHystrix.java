package com.rzj.userconsumer.hystrix;


import com.rzj.b2b.commonmodule.model.User;
import com.rzj.userconsumer.service.UserService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserServiceHystrix implements UserService {

    @Override
    public User login(String uaccount) {
        System.out.println("使用熔断=》user");
        return new User().setUaccount(uaccount).setUname("login==>" + uaccount + "用户不存在，或服务不存在！");
    }

    @Override
    public int register(String upassword, String uname, String usex, String uphone, String unational) {
        System.out.println("使用熔断=》user");
        return 0;
    }

    @Override
    public int addUser(String upassword, String uname, String usex, String uphone, String unational) {
        System.out.println("使用熔断=》user");
        return 0;
    }

    @Override
    public int updateUser(String uaccount, String upassword, String uname, String usex, String uphone, String unational) {
        System.out.println("使用熔断=》user");
        return 0;
    }

    @Override
    public int delUser(String uaccount) {
        System.out.println("使用熔断=》user");
        return 0;
    }

    @Override
    public List<User> findUserByType(Integer utype) {
        System.out.println("使用熔断=》user");
        return (List<User>) new ArrayList<>().set(1,new User().setUname("用户不存在或服务器出错！").setUaccount("空").setUpassword("空").setUsex("空").setUtype(utype));
    }

    @Override
    public User findUserByID(String uaccount) {
        System.out.println("使用熔断=》user");
        return new User().setUname(uaccount+"=》用户不存在或服务器出错！");
    }
}
