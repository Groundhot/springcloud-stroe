package com.rzj.userconsumer.controller;

import com.rzj.userconsumer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class UserController {
    @Autowired
    UserService userService;

    //跳转登录界面
    @GetMapping(value = "/tologin")
    public String tologin() {
        return "login";
    }

    // 跳转注册界面
    @GetMapping(value = "/toregister")
    public String toregister() {
        return "register";
    }

    // 用户登录
    @PostMapping(value = "/login")
    public String login(String uname, String upassword, HttpServletRequest request) {
        if (userService.login(uname) != null) {
            if (userService.login(uname).getUpassword().equals(upassword)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("user", userService.login(uname));  //将登陆者信息存入session
                System.out.println("登录成功");
                if (userService.login(uname).getUtype() == 2) {
                    return "redirect:http://localhost:8894/goods/getAll?uaccount=" + userService.login(uname).getUaccount() +
                            "&upassword=" + userService.login(uname).getUpassword();
                } else {
                    session.setAttribute("utype", userService.login(uname).getUtype());
                    return "redirect:http://localhost:8893/sys/index";
                }
            } else {
                System.out.println("密码错误！");
                return "login";
            }
        }
        // request.setAttribute("mag","账号密码错误");
        System.out.print("账号不存在！");
        return "login";
    }

    //用户注册
    @PostMapping(value = "/register")
    public String register(HttpServletRequest request, String upassword, String uname, String usex, String uphone, String unational) {
        if (userService.register(upassword, uname, usex, uphone, unational) > 0) {
            System.out.print("注册成功");
            return "login";
        }
        System.out.print("注册失败");
        return "register";
    }
}
