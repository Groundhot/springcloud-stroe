package com.rzj.userconsumer.controller;

import com.rzj.b2b.commonmodule.model.User;
import com.rzj.userconsumer.service.UserService;
import com.sun.xml.internal.bind.v2.model.core.MaybeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: store
 * @description: 系统管理
 * @author: 作者
 * @create: 2021-06-19 16:51
 */
@Controller
@RequestMapping(value = "/sys")
public class SysController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/index")
    public String toIndex() {
        return "index";
    }

    @RequestMapping(value = "/quit")
    public String quit(HttpSession session) {
        session.removeAttribute("user");
        return "redirect:http://localhost:8893/admin/tologin";
    }

    @RequestMapping(value = "/welcome")
    public String toMain() {
        return "welcome";
    }

    @RequestMapping(value = "/toUserList")
    public String toUserList() {
        return "redirect:http://localhost:8893/sys/userList";
    }

    @RequestMapping(value = "/toAdminList")
    public String toAdminList() {
        return "redirect:http://localhost:8893/sys/adminList";
    }

    @RequestMapping(value = "/toUser")
    public String toUser() {
        return "";
    }

    @RequestMapping(value = "/toAddUser")
    public String toAddUser() {
        return "user/userAdd";
    }

    @RequestMapping(value = "/toUpdateUser/{uaccount}")
    public String toUpdateUser(@PathVariable(value = "uaccount") String uaccount, HttpServletRequest request) {
        System.out.println("uaccount===>" + String.valueOf(uaccount));
        User user = userService.findUserByID(String.valueOf(uaccount));
        request.setAttribute("user", user);
        return "user/userUpdate";
    }

    @RequestMapping(value = "/delUser")
    @ResponseBody
    public Map<String, String> delUser(@RequestParam(value = "uaccount") String uaccount) {
        Map<String, String> map = new HashMap<>();
        int i = userService.delUser(uaccount);
        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "删除失败，服务器出错！");
            return map;
        }
        map.put("data", "success");
        map.put("msg", "删除成功！");
        return map;
    }

    @RequestMapping(value = "/userList")
    public String userList(HttpServletRequest request) {
        List<User> userList = userService.findUserByType(1);
        request.setAttribute("userList", userList);
        return "user/userList";
    }

    @RequestMapping(value = "/adminList")
    public String adminList(HttpServletRequest request) {
        List<User> userList = userService.findUserByType(2);
        request.setAttribute("adminList", userList);
        return "user/adminList";
    }

    @RequestMapping(value = "/addUser")
    @ResponseBody
    public Map<String, String> addUser(@RequestParam(value = "upassword") String upassword,
                                       @RequestParam(value = "uname") String uname,
                                       @RequestParam(value = "usex") String usex,
                                       @RequestParam(value = "uphone") String uphone,
                                       @RequestParam(value = "unational") String unational) {
        System.out.println("===" + upassword + "==" + uname + "==" + usex + "==" + uphone + "==" + unational);
        Map<String, String> map = new HashMap<>();

        int i = userService.addUser(upassword, uname, usex, uphone, unational);
        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "服务器出错！");
            return map;
        }

        map.put("data", "success");
        map.put("msg", "添加成功!");
        return map;
    }

    @RequestMapping(value = "/updateUser")
    @ResponseBody
    public Map<String, String> updateUser(@RequestParam(value = "uaccount") String uaccount,
                                          @RequestParam(value = "upassword") String upassword,
                                          @RequestParam(value = "uname") String uname,
                                          @RequestParam(value = "usex") String usex,
                                          @RequestParam(value = "uphone") String uphone,
                                          @RequestParam(value = "unational") String unational) {
        System.out.println("===" + upassword + "==" + uname + "==" + usex + "==" + uphone + "==" + unational);
        Map<String, String> map = new HashMap<>();

        int i = userService.updateUser(uaccount, upassword, uname, usex, uphone, unational);

        System.out.println("i==>"+i);

        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "服务器出错！");
            return map;
        }

        map.put("data", "success");
        map.put("msg", "修改成功!");
        return map;
    }


}
