package com.rzj.goodsconsumer.utils;

import java.util.UUID;

/**
 * @program: store
 * @description:
 * @author: 作者
 * @create: 2021-06-21 22:19
 */
public class UUIDUtils {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

}
