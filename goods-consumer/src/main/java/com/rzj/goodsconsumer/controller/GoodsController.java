package com.rzj.goodsconsumer.controller;

import com.rzj.b2b.commonmodule.model.Cart;
import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.b2b.commonmodule.model.Goods;
import com.rzj.goodsconsumer.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    GoodsService goodsService;

    //退出登录并跳转回登录页面
    @RequestMapping(value = "quit")
    public String quit(HttpSession session) {
        session.removeAttribute("uaccount");
        return "redirect:http://localhost:8893/admin/tologin";
    }

    //首页查询全部商品并分类显示
    @RequestMapping(value = "/getAll")
    public String getAll(HttpServletRequest request, String uaccount,
                         String upassword, HttpSession session) {
        session.setAttribute("uaccount", uaccount);
        List<Goods> goods1 = new ArrayList<>();
        List<Goods> goods2 = new ArrayList<>();
        List<Goods> goods3 = new ArrayList<>();
        List<Goods> goods4 = new ArrayList<>();
        List<Goods> oldgoods = goodsService.getAllgoods();
        for (Goods newgoods : oldgoods) {
            System.out.print(newgoods.getTypes());
            if (newgoods.getTypes().equals(1)) {
                goods1.add(newgoods);
            }
            if (newgoods.getTypes().equals(2)) {
                goods2.add(newgoods);
            }
            if (newgoods.getTypes().equals(0)) {
                goods3.add(newgoods);
            }
            if (newgoods.getTypes().equals(3)) {
                goods4.add(newgoods);
            }
        }
        request.setAttribute("goods1", goods1);
        request.setAttribute("goods2", goods2);
        request.setAttribute("goods3", goods3);
        request.setAttribute("goods4", goods4);
        return "index";
    }

    //商品详情-即根据ID查询单个商品
    @RequestMapping(value = "/detail")
    public String detail(HttpServletRequest request, int gid) {

        Goods goods = goodsService.getOnegid(gid);

        Discount discount = goodsService.getOneDiscount(goods.getGname());

        if (discount != null) {
            /*//String applyYear = medicalCard.getEndtime().substring(0,4);
            Calendar date = Calendar.getInstance();
            String year = String.valueOf(date.get(Calendar.YEAR));*/
            double price = goods.gprice;
            BigDecimal price_decimal = new BigDecimal(Double.toString(price));
            price_decimal = discount.getDiscount().multiply(price_decimal).setScale(1,BigDecimal.ROUND_HALF_UP);
            goods.setGprice(price_decimal.intValue());
            //System.out.println("discount==>" + discount.getDiscount().multiply(price_decimal).setScale(1,BigDecimal.ROUND_HALF_UP));
            //System.out.println("discount====>" + String.valueOf(discount.getDiscount().doubleValue() *10));
            //System.out.println("discount======>" + discount.getDiscount().toString());
            //System.out.println("price==>" + price * Integer.valueOf(discount.getDiscount().intValue()));
            System.out.println(goods.toString());
            request.setAttribute("discount", String.valueOf(discount.getDiscount().doubleValue() *10)+" 折");
            request.setAttribute("detail", goods);
            return "detail";
        }

        request.setAttribute("detail", goods);
        return "detail";
    }

    //商品首页搜索-根据名称模糊查找商品
    @RequestMapping(value = "/getOne")
    public String getOne(HttpServletRequest request, String name) {
        List<Goods> goods1 = new ArrayList<>();
        List<Goods> goods2 = new ArrayList<>();
        List<Goods> goods3 = new ArrayList<>();
        List<Goods> goods4 = new ArrayList<>();
        List<Goods> oldgoods = goodsService.getOnegoods(name);
        for (Goods newgoods : oldgoods) {
            System.out.print(newgoods.getTypes());
            if (newgoods.getTypes().equals(1)) {
                goods1.add(newgoods);
            }
            if (newgoods.getTypes().equals(2)) {
                goods2.add(newgoods);
            }
            if (newgoods.getTypes().equals(0)) {
                goods3.add(newgoods);
            }
            if (newgoods.getTypes().equals(3)) {
                goods4.add(newgoods);
            }
        }
        request.setAttribute("goods1", goods1);
        request.setAttribute("goods2", goods2);
        request.setAttribute("goods3", goods3);
        request.setAttribute("goods4", goods4);
        return "index";
    }

    //加入购物车-将商品加入购物车并查询出全部商品如果有商品修改商品数量
    @RequestMapping(value = "cart")
    public String cart(HttpServletRequest request, String name, String price, String che, String number, HttpSession session) {
        String uid = (String) session.getAttribute("uaccount");
        List<Cart> usercart = goodsService.getAllcart(Integer.parseInt(uid));
        int n = Integer.parseInt(price);
        for (int i = 0; i < usercart.size(); i++) {
            if (usercart.get(i).getGoodid() == Integer.parseInt(che)) {
                goodsService.updateCart(Integer.parseInt(number), usercart.get(i).getId());
                return "redirect:/goods/cartAll";
            }
        }
        goodsService.intcart(name, Integer.parseInt(number), n, Integer.parseInt(che), Integer.parseInt(uid));
        return "redirect:/goods/cartAll";
    }

    // 查询购物车全部商品
    @RequestMapping(value = "cartAll")
    public String cartAll(HttpServletRequest request, HttpSession session) {
        String uid = (String) session.getAttribute("uaccount");
        int allPrice = 0, sum = 0;
        List<Cart> cartList = goodsService.getAllcart(Integer.parseInt(uid));
        if (cartList.size() != 0) {
            sum = cartList.size();
            for (Cart cart : cartList) {
                allPrice += cart.getPrice();
            }
        }
        request.setAttribute("carts", cartList);
        request.setAttribute("allPrice", allPrice);
        request.setAttribute("sum", sum);
        return "settle";
    }

    //删除购物车，根据id删除购物车商品
    @RequestMapping(value = "deleteCart")
    public String deleteCart(HttpServletRequest request, String did) {
        goodsService.deleteCart(Integer.parseInt(did));
        return "redirect:/goods/cartAll";
    }

    //添加订单并调用删除购物车（即购买之后删除购物车商品）
    @RequestMapping(value = "/paygoods")
    public String paygoods(HttpServletRequest request, String[] goodid, String[] numaa, HttpSession session, String[] id) {
        System.out.println("goodid==" + goodid.toString() + "numaa==" + numaa.toString() + "id==" + id.toString());
        String uid = (String) session.getAttribute("uaccount");
        if (goodid.length == 0 || numaa.length == 0 || id.length == 0) {
            return "redirect:/goods/cartAll";
        } else {
            for (int i = 0; i < goodid.length; i++) {
                goodsService.insertOrder(goodsService.getOnegid(Integer.parseInt(goodid[i])).gname,
                        Integer.parseInt(numaa[i]),
                        Integer.parseInt(numaa[i]) * goodsService.getOnegid(Integer.parseInt(goodid[i])).gprice,
                        Integer.parseInt(uid));
                goodsService.deleteCart(Integer.parseInt(id[i]));
            }
            request.setAttribute("mag3", "购买成功");
            return "redirect:/goods/getAllorder";
            //项目完成后可以修改跳转到订单页面return "redirect:/goods/getAllorder";
        }
    }

    //根据用户查询登陆者的历史购买记录
    @RequestMapping(value = "/getAllorder")
    public String getAllorder(HttpServletRequest request, HttpSession session) {
        String uid = (String) session.getAttribute("uaccount");
        if (uid.equals(null)) {
            return "redirect:http://localhost:8893/admin/tologin";
        }
        request.setAttribute("order", goodsService.getAllorder(Integer.parseInt(uid)));
        return "order";
    }


}
