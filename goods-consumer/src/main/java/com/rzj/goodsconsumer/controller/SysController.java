package com.rzj.goodsconsumer.controller;

import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.b2b.commonmodule.model.Goods;
import com.rzj.goodsconsumer.service.GoodsService;
import com.rzj.goodsconsumer.utils.FileNameUtils;
import com.rzj.goodsconsumer.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: store
 * @description:
 * @author: 作者
 * @create: 2021-06-21 20:50
 */

@Controller
@RequestMapping(value = "/sys")
public class SysController {

    @Autowired
    GoodsService goodsService;

    @RequestMapping(value = "/toGoodsList")
    public String toGoodsList() {
        return "redirect:http://localhost:8894/sys/goodsList";
    }

    @RequestMapping(value = "/toAddGoods")
    public String toGoodsAdd() {
        return "goods/goodsAdd";
    }

    @RequestMapping(value = "/toUpdateGoods/{gid}")
    public String toGoodsUpdate(@PathVariable(value = "gid") int gid, HttpServletRequest request) {
        Goods goods = goodsService.getOnegid(gid);
        request.setAttribute("goods", goods);
        return "goods/goodsUpdate";
    }

    @RequestMapping(value = "/toDiscountList")
    public String toDiscountList() {
        return "redirect:http://localhost:8894/sys/discountList";
    }

    @RequestMapping(value = "/toDiscountAdd")
    public String toDiscountAdd(HttpServletRequest request) {
        List<Goods> goods = goodsService.getAllgoods();
        request.setAttribute("goods", goods);
        return "discount/discountAdd";
    }

    @RequestMapping(value = "/toDiscountUpdate/{did}")
    public String toDiscountUpdate(@PathVariable(value = "did") int did, HttpServletRequest request) {
        Discount discount = goodsService.getOnedid(did);
        List<Goods> goods = goodsService.getAllgoods();
        request.setAttribute("goods", goods);
        request.setAttribute("discount", discount);
        return "discount/discountUpdate";
    }

    @RequestMapping(value = "/goodsList")
    public String goodsList(HttpServletRequest request) {
        List<Goods> goodsList = goodsService.getAllgoods();
        request.setAttribute("goodsList", goodsList);
        return "goods/goodsList";
    }

    @RequestMapping(value = "/addGoods")
    @ResponseBody
    public Map<String, String> addGoods(@RequestParam(value = "gname") String gname,
                                        @RequestParam(value = "gremain") String gremain,
                                        @RequestParam(value = "gdetails") String gdetails,
                                        @RequestParam(value = "gprice") int gprice,
                                        @RequestParam(value = "types") int types) {
        System.out.println("==>" + gname + "==>" + gremain + "==>" + gdetails + "==>" + gprice + "==>" + types);
        Map<String, String> map = new HashMap<>();
        int i = goodsService.addGoods(gname, gremain, gdetails, gprice, types);
        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "添加失败，服务器出错！");
            return map;
        }
        map.put("data", "success");
        map.put("msg", "添加成功！");
        return map;
    }

    @RequestMapping(value = "/updateGoods")
    @ResponseBody
    public Map<String, String> updateGoods(@RequestParam(value = "gid") int gid,
                                           @RequestParam(value = "gname") String gname,
                                           @RequestParam(value = "gremain") String gremain,
                                           @RequestParam(value = "gdetails") String gdetails,
                                           @RequestParam(value = "gprice") int gprice,
                                           @RequestParam(value = "types") int types) {

        Map<String, String> map = new HashMap<>();

        int i = goodsService.updateGoods(gid, gname, gremain, gdetails, gprice, types);

        //System.out.println("i==>" + i);

        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "服务器出错！");
            return map;
        }

        map.put("data", "success");
        map.put("msg", "修改成功!");
        return map;
    }

    @RequestMapping(value = "/delGoods")
    @ResponseBody
    public Map<String, String> delGoods(@RequestParam(value = "gid") int gid) {
        //System.out.println("gid==>" + gid);
        Map<String, String> map = new HashMap<>();
        int i = goodsService.delGoods(gid);
        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "删除失败，服务器出错！");
            return map;
        }
        map.put("data", "success");
        map.put("msg", "删除成功！");
        return map;
    }

    @RequestMapping(value = "/discountList")
    public String discountList(HttpServletRequest request) {
        List<Discount> discountList = goodsService.getAllDiscount();
        //System.out.println("==>"+discountList.toString());
        request.setAttribute("discountList", discountList);
        return "discount/discountList";
    }

    @RequestMapping(value = "/addDiscount")
    @ResponseBody
    public Map<String, String> addDiscount(@RequestParam(value = "gname") String gname,
                                           @RequestParam(value = "discount") BigDecimal discount,
                                           @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "starTime") Date starTime,
                                           @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "endTime") Date endTime) {

        //System.out.println("==>" + gname + "==>" + discount + "==>" + starTime + "==>" + endTime);
        int i = goodsService.addDiscount(gname, discount, starTime, endTime);
        Map<String, String> map = new HashMap<>();

        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "添加失败，服务器出错！");
            return map;
        }
        map.put("data", "success");
        map.put("msg", "添加成功！");
        return map;

    }

    @RequestMapping(value = "/updateDiscount")
    @ResponseBody
    public Map<String, String> updateDiscount(@RequestParam(value = "did") int did,
                                              @RequestParam(value = "gname") String gname,
                                              @RequestParam(value = "discount") BigDecimal discount,
                                              @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "starTime") Date starTime,
                                              @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "endTime") Date endTime) {

        //System.out.println("==>" + gname + "==>" + discount + "==>" + starTime + "==>" + endTime);
        int i = goodsService.updateDiscount(did, gname, discount, starTime, endTime);
        Map<String, String> map = new HashMap<>();

        if (i == 0) {
            map.put("data", "error");
            map.put("msg", "修改失败，服务器出错！");
            return map;
        }
        map.put("data", "success");
        map.put("msg", "修改成功！");
        return map;

    }


    /**
     * @param file 要上传的文件
     * @return
     */
    @RequestMapping("fileUpload")
    @ResponseBody
    public Map<String, String> upload(@RequestParam(value = "file") MultipartFile file) {

        Map<String, String> map = new HashMap<>();
        // 要上传的目标文件存放路径
        String localPath = "E:/Code_workerpalce/store/goods-consumer/src/main/resources/static/img/goods";
        String sqlPath = "http://localhost:8894/img/goods/";
        // 上传成功或者失败的提示
        String msg = "";
        String data = "";

        //System.out.println("file==>" + file.getOriginalFilename());

        String fileName = FileNameUtils.getFileName(file.getOriginalFilename());
        String fileUrl = sqlPath + fileName;
        //System.out.println("fileUrl==>" + fileUrl);

        //System.out.println("fileName==>" + fileName);

        if (FileUtils.upload(file, localPath, fileName)) {
            // 上传成功，给出页面提示
            data = "success";
            msg = "上传成功！";
            map.put("fileUrl", fileUrl);
        } else {
            data = "error";
            msg = "上传失败！";

        }

        // 显示图片
        map.put("data", data);
        map.put("msg", msg);

        //map.put("fileName", file.getOriginalFilename());

        return map;
    }


}
