package com.rzj.goodsconsumer.service;


import com.rzj.b2b.commonmodule.model.Cart;
import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.b2b.commonmodule.model.Goods;
import com.rzj.b2b.commonmodule.model.Userorder;
import com.rzj.goodsconsumer.hystrix.GoodsServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Component
@FeignClient(value = "b2b-goods-provider", fallback = GoodsServiceHystrix.class)
public interface GoodsService {

    @RequestMapping(value = "/getAllgoods")
    List<Goods> getAllgoods();

    @RequestMapping(value = "/getOnegoods")
    List<Goods> getOnegoods(@RequestParam(value = "gname") String gname);

    @RequestMapping(value = "/getOnegid")
    Goods getOnegid(@RequestParam(value = "gid") int gid);

    @RequestMapping(value = "/addGoods", method = RequestMethod.POST)
    int addGoods(@RequestParam(value = "gname") String gname,
                 @RequestParam(value = "gremain") String gremain,
                 @RequestParam(value = "gdetails") String gdetails,
                 @RequestParam(value = "gprice") int gprice,
                 @RequestParam(value = "types") int types);

    @RequestMapping(value = "/updateGoods", method = RequestMethod.POST)
    int updateGoods(@RequestParam(value = "gid") int gid,
                    @RequestParam(value = "gname") String gname,
                    @RequestParam(value = "gremain") String gremain,
                    @RequestParam(value = "gdetails") String gdetails,
                    @RequestParam(value = "gprice") int gprice,
                    @RequestParam(value = "types") int types);

    @RequestMapping(value = "/delGoods", method = RequestMethod.POST)
    int delGoods(@RequestParam(value = "gid") int gid);

    @RequestMapping(value = "/updateCart")
    int updateCart(@RequestParam(value = "number") int number, @RequestParam(value = "id") int id);

    @RequestMapping(value = "/intcart")
    int intcart(@RequestParam(value = "goodsname") String goodsname, @RequestParam(value = "number") int number, @RequestParam(value = "price") int price, @RequestParam(value = "goodid") int goodid, @RequestParam(value = "uid") int uid);

    @RequestMapping(value = "/getAllcart")
    List<Cart> getAllcart(@RequestParam(value = "uid") int uid);

    @RequestMapping(value = "deleteCart")
    int deleteCart(@RequestParam(value = "gid") int gid);

    @RequestMapping(value = "/insertOrder")
    int insertOrder(@RequestParam(value = "goodsname") String goodsname, @RequestParam(value = "number") int number, @RequestParam(value = "price") int price, @RequestParam(value = "uid") int uid);

    @RequestMapping(value = "/getAllorder")
    List<Userorder> getAllorder(@RequestParam(value = "uid") int uid);

    @RequestMapping(value = "/getAllDiscount", method = RequestMethod.POST)
    List<Discount> getAllDiscount();

    @RequestMapping(value = "/getOneDiscount", method = RequestMethod.POST)
    Discount getOneDiscount(@RequestParam(value = "gname") String gname);

    @RequestMapping(value = "/getOnedid", method = RequestMethod.POST)
    Discount getOnedid(@RequestParam(value = "did") int did);

    @RequestMapping(value = "/addDiscount", method = RequestMethod.POST)
    int addDiscount(@RequestParam(value = "gname") String gname,
                    @RequestParam(value = "discount") BigDecimal discount,
                    @RequestParam(value = "starTime") Date starTime,
                    @RequestParam(value = "endTime") Date endTime);

    @RequestMapping(value = "/updateDiscount", method = RequestMethod.POST)
    int updateDiscount(@RequestParam(value = "did") int did,
                       @RequestParam(value = "gname") String gname,
                       @RequestParam(value = "discount") BigDecimal discount,
                       @RequestParam(value = "starTime") Date starTime,
                       @RequestParam(value = "endTime") Date endTime);

    @RequestMapping(value = "/delDiscount", method = RequestMethod.POST)
    int delDiscount(@RequestParam(value = "did") int did);


}

