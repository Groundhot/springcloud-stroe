package com.rzj.goodsconsumer.hystrix;



import com.rzj.b2b.commonmodule.model.Cart;
import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.b2b.commonmodule.model.Goods;
import com.rzj.b2b.commonmodule.model.Userorder;
import com.rzj.goodsconsumer.service.GoodsService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GoodsServiceHystrix implements GoodsService {

    @Override
    public List<Goods> getAllgoods() {
        return null;
    }

    @Override
    public List<Goods> getOnegoods(String gname) {
        return null;
    }

    @Override
    public Goods getOnegid(int gid) {
        return null;
    }

    @Override
    public int addGoods(String gname, String gremain, String gdetails, int gprice, int types) {
        return 0;
    }

    @Override
    public int updateGoods(int gid, String gname, String gremain, String gdetails, int gprice, int types) {
        return 0;
    }

    @Override
    public int delGoods(int gid) {
        return 0;
    }

    public int updateCart(int number,int id) {
        return 0;
    }

    public int intcart(String goodsname,int number,int price,int goodid,int uid){
        return 0;
    }

    public List<Cart> getAllcart(int uid) {
        return null;
    }

    @Override
    public int deleteCart(int gid) {
        return 0;
    }

    @Override
    public int insertOrder(String goodsname, int number, int price, int uid) {
        return 0;
    }

    @Override
    public List<Userorder> getAllorder(int uid) {
        return null;
    }

    @Override
    public List<Discount> getAllDiscount() {
        return null;
    }

    @Override
    public Discount getOneDiscount(String gname) {
        return null;
    }

    @Override
    public Discount getOnedid(int did) {
        return null;
    }

    @Override
    public int addDiscount(String gname, BigDecimal discount, Date starTime, Date endTime) {
        return 0;
    }

    @Override
    public int updateDiscount(int did, String gname, BigDecimal discount, Date starTime, Date endTime) {
        return 0;
    }

    @Override
    public int delDiscount(int did) {
        return 0;
    }

}
