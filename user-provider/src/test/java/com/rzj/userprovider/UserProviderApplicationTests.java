package com.rzj.userprovider;

import com.rzj.b2b.commonmodule.model.User;
import com.rzj.userprovider.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UserProviderApplicationTests {

    @Autowired
    UserController userController;

    @Test
    void contextLoads() {
        User user = userController.login("1");
        System.out.println("user===="+user);
        System.out.println("type===="+user.getUtype());
    }

    @Test
    void addUser(){
        //userController.addUser("2", "2", "2");
    }

    @Test
    void updateUser(){
        //userController.updateUser("10029","3","2","女");
    }

    @Test
    void delUser() {
        userController.delUser("2");
    }

    @Test
    void userList() {
        List<User> userList = userController.findUserByType(1);
        System.out.println("userlist====="+userList.toString());
    }

    @Test
    void user() {
        /*User user = userController.findUserByName("1");
        System.out.println("user==="+user.toString());*/
    }

}
