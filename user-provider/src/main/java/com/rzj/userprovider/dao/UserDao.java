package com.rzj.userprovider.dao;


import com.rzj.b2b.commonmodule.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao {
    //用户注册接口
    int register(@Param(value = "upassword") String upassword,
                 @Param(value = "uname") String uname,
                 @Param(value = "usex") String usex,
                 @Param(value = "uphone") String uphone,
                 @Param(value = "unational") String unational);

    int addUser(@Param(value = "upassword") String upassword,
                @Param(value = "uname") String uname,
                @Param(value = "usex") String usex,
                @Param(value = "uphone") String uphone,
                @Param(value = "unational") String unational);

    List<User> findUserByType(@Param(value = "utype") Integer utype);

    User findUserByUserID(@Param(value = "uaccount") String uaccount);

    int delUser(@Param(value = "uaccount") String uaccount);

    int updateUser(@Param(value = "uaccount") String uaccount,
                   @Param(value = "upassword") String upassword,
                   @Param(value = "uname") String uname,
                   @Param(value = "usex") String usex,
                   @Param(value = "uphone") String uphone,
                   @Param(value = "unational") String unational);

    User login(@Param(value = "uname") String uname);//用户登录接口;
}
