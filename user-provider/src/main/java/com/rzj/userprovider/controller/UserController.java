package com.rzj.userprovider.controller;



import com.rzj.b2b.commonmodule.model.User;
import com.rzj.userprovider.dao.UserDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class UserController {

    @Resource
    UserDao userDao;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public int register(@RequestParam(value = "upassword") String upassword,
                        @RequestParam(value = "uname") String uname,
                        @RequestParam(value = "usex") String usex,
                        @RequestParam(value = "uphone") String uphone,
                        @RequestParam(value = "unational") String unational) {
        return userDao.register(upassword, uname, usex, uphone, unational);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public User login(@RequestParam("uname") String uname) {
        return userDao.login(uname);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public int addUser(@RequestParam(value = "upassword") String upassword,
                       @RequestParam(value = "uname") String uname,
                       @RequestParam(value = "usex") String usex,
                       @RequestParam(value = "uphone") String uphone,
                       @RequestParam(value = "unational") String unational) {
        return userDao.addUser(upassword, uname, usex, uphone, unational);
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public int updateUser(@RequestParam(value = "uaccount") String uaccount,
                          @RequestParam(value = "upassword") String upassword,
                          @RequestParam(value = "uname") String uname,
                          @RequestParam(value = "usex") String usex,
                          @RequestParam(value = "uphone") String uphone,
                          @RequestParam(value = "unational") String unational) {
        System.out.println("UserController.updateUser");
        return userDao.updateUser(uaccount, upassword, uname, usex, uphone, unational);
    }

    @RequestMapping(value = "/delUser", method = RequestMethod.POST)
    public int delUser(@RequestParam(value = "uaccount") String uaccount) {
        return userDao.delUser(uaccount);
    }

    @RequestMapping(value = "/userList", method = RequestMethod.POST)
    public List<User> findUserByType(@RequestParam(value = "utype") Integer utype) {
        return userDao.findUserByType(utype);
    }

    @RequestMapping(value = "/user/{uaccount}", method = RequestMethod.POST)
    public User findUserByID(@PathVariable(value = "uaccount") String uaccount) {
        return userDao.findUserByUserID(uaccount);
    }

}

