package com.rzj.goodsprovider.dao;

import com.google.inject.internal.asm.$AnnotationVisitor;
import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.b2b.commonmodule.model.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @program: store
 * @description:
 * @author: 作者
 * @create: 2021-06-22 16:55
 */

@Mapper
public interface DiscountDao {

    //查询所有商品
    List<Discount> getAllDiscount();

    //根据商品名称模糊查找
    Discount getOneDiscount(@Param(value = "gname") String gname);

    //根据id查询单个商品详情
    Discount getOnedid(@Param(value = "did") int did);

    int addDiscount(@Param(value = "gname") String gname,
                    @Param(value = "discount") BigDecimal discount,
                    @Param(value = "starTime") Date starTime,
                    @Param(value = "endTime") Date endTime);

    int updateDiscount(@Param(value = "did") int did,
                       @Param(value = "gname") String gname,
                       @Param(value = "discount") BigDecimal discount,
                       @Param(value = "starTime") Date starTime,
                       @Param(value = "endTime") Date endTime);

    int delDiscount(@Param(value = "did") int did);

}
