package com.rzj.goodsprovider.dao;



import com.rzj.b2b.commonmodule.model.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GoodsDao {
    //查询所有商品
    List<Goods> getAllgoods();

    //根据商品名称模糊查找
    List<Goods> getOnegoods(@Param(value = "gname") String gname);

    //根据id查询单个商品详情
    Goods getOnegid(@Param(value = "gid") int gid);

    int addGoods(@Param(value = "gname") String gname,
                 @Param(value = "gremain") String gremain,
                 @Param(value = "gdetails") String gdetails,
                 @Param(value = "gprice") int gprice,
                 @Param(value = "types") int types);

    int updateGoods(@Param(value = "gid") int gid,
                    @Param(value = "gname") String gname,
                    @Param(value = "gremain") String gremain,
                    @Param(value = "gdetails") String gdetails,
                    @Param(value = "gprice") int gprice,
                    @Param(value = "types") int types);

    int delGoods(@Param(value = "gid") int gid);

}
