package com.rzj.goodsprovider.controller;



import com.rzj.b2b.commonmodule.model.Goods;
import com.rzj.goodsprovider.dao.GoodsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GoodsController {

    @Autowired
    GoodsDao goodsDao;

    @RequestMapping(value = "/getAllgoods")
    public List<Goods> getAllgoods() {
        return goodsDao.getAllgoods();
    }

    @RequestMapping(value = "/getOnegoods")
    public List<Goods> getOnegoods(@RequestParam(value = "gname") String gname) {
        return goodsDao.getOnegoods(gname);
    }

    @RequestMapping(value = "/getOnegid")
    public Goods getOnegid(@RequestParam(value = "gid") int gid) {
        return goodsDao.getOnegid(gid);
    }

    @RequestMapping(value = "/addGoods", method = RequestMethod.POST)
    public int addGoods(@RequestParam(value = "gname") String gname,
                        @RequestParam(value = "gremain") String gremain,
                        @RequestParam(value = "gdetails") String gdetails,
                        @RequestParam(value = "gprice") int gprice,
                        @RequestParam(value = "types") int types) {
        return goodsDao.addGoods(gname, gremain, gdetails, gprice, types);
    }

    @RequestMapping(value = "/updateGoods", method = RequestMethod.POST)
    public int updateGoods(@RequestParam(value = "gid") int gid,
                           @RequestParam(value = "gname") String gname,
                           @RequestParam(value = "gremain") String gremain,
                           @RequestParam(value = "gdetails") String gdetails,
                           @RequestParam(value = "gprice") int gprice,
                           @RequestParam(value = "types") int types) {
        return goodsDao.updateGoods(gid, gname, gremain, gdetails, gprice, types);
    }

    @RequestMapping(value = "/delGoods", method = RequestMethod.POST)
    public int delGoods(@RequestParam(value = "gid") int gid) {
        return goodsDao.delGoods(gid);
    }

}
