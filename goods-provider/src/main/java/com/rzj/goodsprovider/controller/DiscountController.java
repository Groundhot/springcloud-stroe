package com.rzj.goodsprovider.controller;

import com.rzj.b2b.commonmodule.model.Discount;
import com.rzj.goodsprovider.dao.DiscountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @program: store
 * @description:
 * @author: 作者
 * @create: 2021-06-22 17:10
 */

@RestController
public class DiscountController {

    @Autowired
    DiscountDao discountDao;

    @RequestMapping(value = "/getAllDiscount", method = RequestMethod.POST)
    public List<Discount> getAllDiscount() {
        return discountDao.getAllDiscount();
    }

    @RequestMapping(value = "/getOneDiscount", method = RequestMethod.POST)
    public Discount getOneDiscount(@RequestParam(value = "gname") String gname) {
        if (discountDao.getOneDiscount(gname) == null) {
            return null;
        }
        return discountDao.getOneDiscount(gname);
    }

    @RequestMapping(value = "/getOnedid", method = RequestMethod.POST)
    public Discount getOnedid(@RequestParam(value = "did") int did) {
        return discountDao.getOnedid(did);
    }

    @RequestMapping(value = "/addDiscount", method = RequestMethod.POST)
    public int addDiscount(@RequestParam(value = "gname") String gname,
                           @RequestParam(value = "discount") BigDecimal discount,
                           @RequestParam(value = "starTime") Date starTime,
                           @RequestParam(value = "endTime") Date endTime) {
        return discountDao.addDiscount(gname, discount, starTime, endTime);
    }

    @RequestMapping(value = "/updateDiscount", method = RequestMethod.POST)
    public int updateDiscount(@RequestParam(value = "did") int did,
                              @RequestParam(value = "gname") String gname,
                              @RequestParam(value = "discount") BigDecimal discount,
                              @RequestParam(value = "starTime") Date starTime,
                              @RequestParam(value = "endTime") Date endTime) {
        return discountDao.updateDiscount(did, gname, discount, starTime, endTime);
    }

    @RequestMapping(value = "/delDiscount", method = RequestMethod.POST)
    public int delDiscount(@RequestParam(value = "did") int did) {
        return discountDao.delDiscount(did);
    }

}
